#pragma once

#include <string>
#include <iostream>
class Desktop
{
public:
	Desktop();
	~Desktop();

	virtual void getType() = 0;
};
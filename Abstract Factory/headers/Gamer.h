#pragma once
#include "Laptop.h"
class Gamer :
	public Laptop
{
public:
	Gamer();
	~Gamer();

	virtual void getType() { std::cout << "Type Gamer" << std::endl; }
};


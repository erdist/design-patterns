#pragma once
#include "AbstractFactory.h"
#include "Desktop.h"
#include "Assembled.h"
#include "Brand.h"
#include "AllInOne.h"
class DesktopFactory :
	public AbstractFactory
{
public:
	DesktopFactory();
	~DesktopFactory();

	virtual Desktop* createDesktop(std::string type);
	virtual Laptop* createLaptop(std::string type) { return NULL; };

};


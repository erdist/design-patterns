#pragma once
#include "Laptop.h"
class Ultrabook :
	public Laptop
{
public:
	Ultrabook();
	~Ultrabook();

	virtual void getType() { std::cout << "Type Ultrabook" << std::endl; }
};


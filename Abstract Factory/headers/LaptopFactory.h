#pragma once
#include "AbstractFactory.h"
#include "Laptop.h"
#include "Gamer.h"
#include "Ultrabook.h"
#include "Casual.h"
class LaptopFactory :
	public AbstractFactory
{
public:
	LaptopFactory();
	~LaptopFactory();

	virtual Laptop* createLaptop(std::string type);
	virtual Desktop* createDesktop(std::string type) { return NULL; };
};


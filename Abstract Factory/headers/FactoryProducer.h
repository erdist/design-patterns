#pragma once

#include "AbstractFactory.h"
#include "DesktopFactory.h"
#include "LaptopFactory.h"
class FactoryProducer
{
public:
	FactoryProducer();
	~FactoryProducer();

	static AbstractFactory* getFactory(std::string choice);

};


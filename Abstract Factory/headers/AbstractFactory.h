#pragma once

#include <string>
#include <iostream>
#include "Desktop.h"
#include "Laptop.h"
#include "Brand.h"
class AbstractFactory
{
public:
	AbstractFactory() {}
	~AbstractFactory(){}

	virtual Desktop* createDesktop(std::string type) = 0;
	virtual Laptop* createLaptop(std::string type) = 0;
};


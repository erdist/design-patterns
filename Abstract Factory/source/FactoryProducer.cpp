#include "FactoryProducer.h"


FactoryProducer::FactoryProducer()
{
}


FactoryProducer::~FactoryProducer()
{
}

AbstractFactory * FactoryProducer::getFactory(std::string choice)
{
	if (!choice.compare("Desktop"))
		return new DesktopFactory();
	else if (!choice.compare("Laptop"))
		return new LaptopFactory();

	return NULL;
}

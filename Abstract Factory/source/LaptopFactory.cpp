#include "LaptopFactory.h"


LaptopFactory::LaptopFactory():AbstractFactory()
{
	std::cout << "Laptop Factory Constructor" << std::endl;

}


LaptopFactory::~LaptopFactory()
{
}

Laptop * LaptopFactory::createLaptop(std::string type)
{
	if (!type.compare("Gamer"))
		return new Gamer();
	else if (!type.compare("Ultrabook"))
		return new Ultrabook();
	else if (!type.compare("Casual"))
		return new Casual();

	return NULL;
}

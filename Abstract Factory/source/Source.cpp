#include "FactoryProducer.h"
#include <stdio.h>
#include <iostream>
#include "DesktopFactory.h"

int main()
{
	AbstractFactory* factory1 = FactoryProducer::getFactory("Laptop");
	AbstractFactory* factory2 = FactoryProducer::getFactory("Desktop");
	

	Laptop* product1 = factory1->createLaptop("Casual");
	Desktop* product2 = factory2->createDesktop("AllInOne");


	product1->getType();
	product2->getType();

	return 0;
}
#include "DesktopFactory.h"


DesktopFactory::DesktopFactory():AbstractFactory()
{
	std::cout << "Desktop Factory Constructor" << std::endl;
}


DesktopFactory::~DesktopFactory()
{
}

Desktop* DesktopFactory::createDesktop(std::string type)
{
	if (!type.compare("Assembled"))
		return new Assembled();
	else if (!type.compare("Brand"))
		return new Brand();
	else if (!type.compare("AllInOne"))
		return new AllInOne();

	return NULL;
}
